#include <stdio.h>

int main() {
    int numero;
    printf("Introduza un entero: ");
    scanf("%d", &numero);

    if (numero < 0) {
        printf("El numero introducido es %d. \n", numero);
    }

    printf("Hemos terminado!");

    return 0;
}