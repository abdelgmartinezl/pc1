#include <stdio.h>

int main() {
    int numero;
    printf("Introduza un entero: ");
    scanf("%d", &numero);

    if (numero > 0) {
        printf("%d es positivo", numero);
    } else if (numero == 0) {
        printf("%d es cero", numero);
    } else {
        printf("%d es negativo", numero);
    }

    return 0;
}