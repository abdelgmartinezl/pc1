#include <stdio.h>

int main() {
    char operador;
    double n1, n2;

    printf("Introduzca un operador (+, -, *, /): ");
    scanf("%c", &operador);
    printf("Introduzca dos operandos: ");
    scanf("%lf %lf", &n1, &n2);

    switch (operador) {
        case '+':
            printf("%.1lf + %.1lf = %.1lf", n1, n2, n1+n2);
            break;
        case '-':
            printf("%.1lf - %.1lf = %.1lf", n1, n2, n1-n2);
            break;
        case '*':            
            printf("%.1lf * %.1lf = %.1lf", n1, n2, n1*n2);
            break;
        case '/':            
            printf("%.1lf / %.1lf = %.1lf", n1, n2, n1/n2);
            break;
        default:
            printf("No hay operador disponible...");
    }

    return 0;
}