#include <stdio.h>

int main() {
    float numero, suma = 0;

    do {
        printf("Introduzca un numero: ");
        scanf("%f", &numero);
        suma += numero;
    } while (numero != 0.0);

    printf("Suma = %.2f", suma);

    return 0;
}