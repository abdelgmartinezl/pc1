#include <stdio.h>

int main() {
    int numero;
    printf("Introduza un entero: ");
    scanf("%d", &numero);

    if (numero % 2 == 0) {
        printf("%d es un numero par", numero);
    } else {
        printf("%d es un numero impar", numero);
    }

    return 0;
}