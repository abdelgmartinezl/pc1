#include <stdio.h>

int main() {
    int numero;
    printf("Introduza un entero: ");
    scanf("%d", &numero);

    if (numero > 0) {
        printf("%d es positivo", numero);
        if (numero % 2 == 0) {
            printf("\nEs un par");
        } else {
            printf("\nEs un impar");
        }
    } else if (numero == 0) {
        printf("%d es cero", numero);
    } else {
        printf("%d es negativo", numero);
    }

    return 0;
}