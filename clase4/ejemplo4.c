#include <stdio.h>

const int CIUDAD = 2;
const int SEMANA = 7;

int main() {
    int temperatura[CIUDAD][SEMANA];

    for (int i = 0; i < CIUDAD; ++i) {
        for (int j = 0; j < SEMANA; ++j) {
            printf("Ciudad %d, Dia %d: ", i+1, j+1);
            scanf("%d", &temperatura[i][j]);
        }
    }

    printf("Mostrando valores");
    for (int i = 0; i < CIUDAD; ++i) {
        for (int j = 0; j < SEMANA; ++j) {
            printf("Ciudad %d, Dia %d = %d\n", i+1, j+1, temperatura[i][j]);
        }
    }

    return 0;
}