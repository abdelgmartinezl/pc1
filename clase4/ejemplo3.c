#include <stdio.h>

int main() {
    int puntos[10], i, n, total = 0, promedio;

    printf("Introducir el numero de elementos: ");
    scanf("%d", &n);

    for (i=0; i<n; ++i) {
        printf("Introducir número %d: ", i+1);
        scanf("%d", &puntos[i]);

        total += puntos[i];
    }

    promedio = total/n;
    printf("Promedio = %d", promedio);

    return 0;
}