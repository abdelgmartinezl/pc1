#include <stdio.h>
int checarNumeroPrimo(int n);

int main () {
    int n, band;
    printf("Introducir número positivo: ");
    scanf("%d", &n);

    band = checarNumeroPrimo(n);

    if (band == 1) {
        printf("%d no es primo", n);
    } else {
        printf("%d es un numero primo", n);
    }
    
    return 0;
}

int checarNumeroPrimo(n) {
    int i;
    for (i=2; i<=n/2; ++i)  {
        if (n%i == 0) {
            return 1;
        }
    }
    return 0;
}