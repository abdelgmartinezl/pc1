#include <stdio.h>

int main() {
    int var = 20;
    int *ip;
    
    ip = &var;

    printf("Direccion de variable var:  %x\n", &var);
    printf("Direcciona almacenada de variable ip: %x\n", ip);
    printf("Direccion de varible *ip: %d\n", *ip);

    return 0;
}
