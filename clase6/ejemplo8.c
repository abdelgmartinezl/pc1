#include <stdio.h>

int main() {
    int* pa, a;

    a = 17;
    printf("Direccion de a: %p\n", &a);
    printf("Valor de a: %d\n]n", a);

    pa = &a;
    printf("Direccion de puntero pa: %p\n", pa);
    printf("Contenido de puntero pa: %d\n]n", *pa);

    a = 6;
    printf("Direccion de puntero pa: %p\n", pa);
    printf("Contenido de puntero pa: %d\n]n", *pa);

    *pa = 1;
    printf("Direccion de a: %p\n", &a);
    printf("Valor de a: %d\n]n", a);

    return 0;
}
