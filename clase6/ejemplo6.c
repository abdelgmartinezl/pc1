#include <stdio.h>
void saludar();

int main() {
    saludar();
    saludar();
    saludar();

    return 0;
}

void saludar() {
    int a = 1;
    static int b = 10;
    
    printf("\n\na = %d", a);
    printf("\nb = %d", b);
    
    a++;
    b++;
}