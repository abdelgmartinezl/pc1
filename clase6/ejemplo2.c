#include <stdio.h>
void checarNumeroPrimo();

void checarNumeroPrimo() {
    int n, i, band = 0;
    
    printf("Introducir entero positivo: ");
    scanf("%d", &n);

    for (i=2; i <= n/2; ++i) {
        if (n%i == 0) {
            band = 1;
        }
    }

    if (band == 1) {
        printf("%d no es primo.", n);
    } else {
        printf("%d es primo,", n);
    }
}

int main() {
    checarNumeroPrimo();
    return 0;
}