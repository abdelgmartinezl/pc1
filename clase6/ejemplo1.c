#include <stdio.h>
int sumarNumeros(int x, int y);

int main() {
    int n1, n2, suma;

    printf("Introduzca dos numeros: ");
    scanf("%d %d", &n1, &n2);

    suma = sumarNumeros(n1, n2);
    printf("Suma = %d", suma);

    return 0;
}

int sumarNumeros(int x, int y) {
    int resultado;
    resultado = x + y;
    return resultado;
}