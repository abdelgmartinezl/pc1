#include <stdio.h>
#include <stdlib.h>

int main() {
    int n, i, *p, suma = 0;

    printf("Introducir numero de elementos: ");
    scanf("%d", &n);

    p = (int*) malloc(n * sizeof(int));

    if (p == NULL) {
        printf("Error! Memoria no asignada.");
        exit(0);
    }

    printf("Introducir elementos: ");
    for (i = 0; i < n; i++) {
        scanf("%d", p + i);
        suma += *(p + i);
    }

    printf("Suma = %d", suma);

    free(p);

    return 0;
}