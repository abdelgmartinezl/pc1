#include <stdio.h>

void chotear(int x) {
    printf("Habla lapecillo %d\n", x);
}

int main() {
    void (*chotear_ptr)(int) = &chotear;

    (*chotear_ptr)(100);
    
    return 0;
}