#include <stdio.h>
#include <stdlib.h>

typedef struct Contacto {
   char nombre[100];
   int numero;
   char correo[100];
} Contacto;

int main() {
    Contacto * miscontactos;
    miscontactos = (Contacto*) malloc(10 * sizeof(Contacto));

    int posicion, x;
    
    for (x = 0; x < 5; x++) {
        printf("Contacto: ");
        scanf("%s %d %s", &miscontactos[x].nombre, &miscontactos[x].numero, &miscontactos[x].correo );
    }

    for (x = 0; x < 5; x++) {
        printf("\nContacto %d: %s %d %s", x+1 , miscontactos[x].nombre, miscontactos[x].numero, miscontactos[x].correo);
    }

    printf("\n\nElemento a Borrar: ");
    scanf("%d", &posicion);

    if (posicion >= 5 || posicion < 0) {
        printf("No se ha eliminado nada...");
    } else {
        for (x = posicion; x <= 4; x++) {
            miscontactos[x] = miscontactos[x+1];
        }
    }

    for (x = 0; x < 5; x++) {
        if (miscontactos[x].numero != 0) {
            printf("\nContacto %d: %s %d %s", x+1 , miscontactos[x].nombre, miscontactos[x].numero, miscontactos[x].correo);
        }
    }
    
    printf("\n\nCiao!");

    free(miscontactos);

    return 0;
}