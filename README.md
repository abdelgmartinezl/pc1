# Programación de Computadoras I

Ejemplos de códigos utilizados para las clases de Programación de Computadoras I, cuyo lenguaje de programación es C.

## Módulos
Clase 3 - Tipos de Datos, Variables y Operadores Aritméticos
Clase 4 - Arreglos, Arreglos Multidimensionales
Clase 5 - Condiciones, Cadenas, Ciclos
Clase 6 - Funciones, Variables Estáticas, Punteros
Clase 7 - Estructuras, Pase por Referencia
Clase 8 - Asignación Dinámica, Arreglo Punteros, Función Puntero