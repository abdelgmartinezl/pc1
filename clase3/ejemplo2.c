#include <stdio.h>

int main() {
	int x, y, z;
	printf("Introduzca el valor de X: ");
	scanf("%d", &x);
	printf("\nIntroduzca el valor de Y: ");
	scanf("%d", &y);
	z = x + y;
	printf("%d + %d = %d\n", x, y, z);
	return 0;
}
