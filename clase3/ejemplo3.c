#include <stdio.h>

int main() {
	float salario;
	char nombre[20];

	printf("Nombre: ");
	scanf("%s", nombre);
	printf("Salario: ");
	scanf("%f", &salario);

	printf("\n%s, gana un salario de $%.2f", nombre, salario);
	return 0;
}
