#include <stdio.h>

int main() {
	int a = 9;
	int b = 4;
	int c;
	float d = 100;

	c = a + b;
	printf("a+b = %d \n", c);
	c = a - b;
	printf("a-b = %d \n", c);
	c = a * b;
	printf("a*b = %d \n", c);
	c = a / b;
	printf("a/b = %d \n", c);
	c = a % b;
	printf("Residuo de a cuando se divide entre b = %d \n", c);
	
	printf("++d = %f \n", ++d);
	printf("--d = %f \n", --d);

	c = (a * b) / (a + b) + d;
	printf("(a*b)/(a+b)+d = %d", c);

	return 0;
}
