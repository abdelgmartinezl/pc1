#include <stdio.h>

typedef struct {
    char * nombre;
    int edad;
} persona;

int main() {
    persona juan;

    juan.nombre = "Juan";
    juan.edad = 35;

    printf("%s tiene %d años de edad.", juan.nombre, juan.edad);

    return 0;
}