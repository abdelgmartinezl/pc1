#include <stdio.h>
#include <stdlib.h>

typedef struct {
     int x;
     int y;
} punto;

int main() {
    punto * mispuntos;
    int n, i;

    printf("Introduzca la cantidad de puntos: ");
    scanf("%d", &n);

    mispuntos = (punto*)malloc(n * sizeof(punto));
    
    if (mispuntos == NULL) {
        printf("ERROR::Memoria no asignada\n");
        exit(0);
    } else {
        for (i = 0; i < n; i++) {
            printf("\n\nCompletando el punto %d\n", i+1);

            printf("Coordenada X = ");
            scanf("%d", &mispuntos[i].x);
            printf("Coordenada Y = ");
            scanf("%d", &mispuntos[i].y);   
        }

        printf("\n\nMis Puntos\n\n");
        for (i = 0; i < n; i++) {
            printf("Punto %d, X = %d\n", i+1, mispuntos[i].x);
            printf("Punto %d, Y = %d\n\n", i+1, mispuntos[i].y);
        }
    }

    free(mispuntos);
    return 0;
}