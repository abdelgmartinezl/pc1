#include <stdio.h>
#include <stdlib.h>

void ladrar();
void comer();

typedef struct {
     char nombre[25];
     char raza[25];
     int vida;
} perro;

void ladrar(perro* p) {
    printf("Woof! Woof!");
    p->vida--;
}

void comer(perro* p) {
    printf("Nom Nom!");
    p->vida++;
}

int main() {
    int opcion = 0;
    perro * lassie;
    lassie = (perro*)malloc(sizeof(perro));
   
    while (opcion != 5) {
        printf("\n\n\n-------MENU-------");
        printf("\n1. Adoptar Perro");
        printf("\n2. Ver Puntos de Vida");
        printf("\n3. Ladrar");
        printf("\n4. Comer");
        printf("\n5. Salir");
        printf("\n--> Opcion : ");
        scanf("%d", &opcion);

        if (opcion == 1) {
            printf("Nombre del Perro: ");
            scanf("%s", lassie->nombre);
            printf("Raza del Perro: ");
            scanf("%s", lassie->raza);
            lassie->vida = 5;
        } else if (opcion == 2) {
            printf("\nPuntos de Vida: %d\n", lassie->vida);
        } else if (opcion == 3) {
            ladrar(lassie);
        } else if (opcion == 4) {
            comer(lassie);
        } else if (opcion == 5) {
            printf("Puntos de Vida: %d\n", lassie->vida);
            printf("Hasta luego!\n");
        } else {
            printf("ERROR::Opcion no valida");
        }
    } 

    free(lassie);
    return 0;
}