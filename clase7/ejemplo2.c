#include <stdio.h>

typedef struct {
    char * nombre;
    int edad;
} persona;

void cumpleanos(persona * p);

void cumpleanos(persona * p) {
    p->edad++;
}

int main() {
    persona juan;
    juan.nombre = "Juan";
    juan.edad = 35;
    
    printf("%s tiene %d años de edad.\n", juan.nombre, juan.edad);
    cumpleanos(&juan);
    printf("Feliz Cumpleaños! %s tiene ahora %d años de edad");

    return 0;
}