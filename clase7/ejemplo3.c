#include <stdio.h>
#include <stdlib.h>

typedef struct {
     int x;
     int y;
} punto;

int main() {
    punto * mipuntos;
    mipuntos = (punto*)malloc(sizeof(punto));
    
    mipuntos->x = 10;
    mipuntos->y = 30;
    printf("Coordenadas: %d %d\n", mipuntos->x, mipuntos->y);
    
    free (mipuntos);
    return 0;
}